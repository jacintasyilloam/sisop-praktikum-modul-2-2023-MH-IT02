#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>

// LOG ENTRY
void createlog(const char *path)
{
  time_t t;
  struct tm *time_details;

  // get current time
  time(&t);
  time_details = localtime(&t);

  char formatted[50];

  // format the time string as requested
  strftime(formatted, sizeof(formatted), "[%Y-%m-%d %H:%M:%S]", time_details);

  FILE *log_file = fopen("/home/suguru/cleaner.log", "a"); // open log file
  if (log_file)
  {
    // write log message to log file
    fprintf(log_file, "%s '%s' has been removed.\n", formatted, path);
    fclose(log_file);
  }
}

// REMOVE SUSSY FILES
void filelist(const char *path)
{
  printf("Run filelists..\n");
  DIR *directory;         // used to represent the opened directory
  struct dirent *txtfile; // holds info abt directory entry (file/subdir)

  directory = opendir(path); // open directory according to path

  if (directory != NULL)
  {

    if (readdir(directory) == NULL) // check if readdir works
    {
      printf("error readdir");
      return;
    }

    while (txtfile = readdir(directory)) // loop directory until theres no more entry
    {
      char file_path[1000]; // Store the full path to the file
      snprintf(file_path, sizeof(file_path), "%s/%s", path, txtfile->d_name);

      if (txtfile->d_type == DT_REG) // check if entry is a regular file
      {
        printf("found file\n");

        FILE *file = fopen(file_path, "r"); // open file

        if (file) 
        {
          char content[1000]; // array to store content
          while (fgets(content, sizeof(content), file))
          {
            if (strstr(content, "SUSPICIOUS") != NULL)
            {
              remove(file_path); // remove if theres sus
              createlog(file_path); // add log
              break;
            }
          }
        }
        fclose(file);
      }
      else if (txtfile->d_type == DT_DIR) // check if directory
      {
        printf("found a directory\n");
        if (strcmp(txtfile->d_name, ".") == 0 || strcmp(txtfile->d_name, "..") == 0) 
          continue; // skip current directory (.) and parent dir (..)

        filelist(file_path); // recursive
      }
    }
    closedir(directory);
  }
  else
  {
    printf("Error opendir");
    return;
  }
}

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    exit(EXIT_FAILURE);
  }

  pid_t pid, sid; // variable to store pid

  pid = fork(); // store PID of Child Process

  // when fork fails
  if (pid < 0)
  {
    exit(EXIT_FAILURE);
  }

  // When fork succeeds
  // (variable pid is PID of child process)
  if (pid > 0)
  {
    exit(EXIT_SUCCESS);
  }

  umask(0); // to gain access to file created by daemon

  // give child process unique session id
  sid = setsid();
  if (sid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (chdir("/") < 0)
  {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO); 
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1)
  {
    filelist(argv[1]); // run specified path
    sleep(30); // wait 30 secs before repeating the process   
  }

  return 0;
}