#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <signal.h>
#include <pwd.h>
#include <time.h>
#include <sys/wait.h>
#include <stdbool.h>

const char *convertname(const char *filename)
{
    struct stat file_info;

    if (stat(filename, &file_info) == 0)
    {
        // Get the username from the user ID
        struct passwd *owner_info = getpwuid(file_info.st_uid);
        if (owner_info != NULL)
        {
            printf("Owner of %s is %s\n", filename, owner_info->pw_name);
            return owner_info->pw_name;
        }
        else
        {
            perror("getpwuid");
        }
    }
    else
    {
        perror("stat");
    }

    return NULL;
}

void createlog(const char *file)
{
    time_t t;
    struct tm *time_details;

    time(&t);
    time_details = localtime(&t);

    char formatted[50];
    strftime(formatted, sizeof(formatted), "%Y-%m-%d %H:%M:%S", time_details);

    FILE *log_file = fopen("/home/suguru/virus.log", "a");
    if (log_file)
    {
        printf("log is created");
        fprintf(log_file, "[%s][%s] - %s - Moved to quarantine\n", convertname(file), formatted, file);
        fclose(log_file);
    }
}

void donload()
{
    pid_t tes;

    tes = fork();
    if (tes < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (tes == 0)
    {
        char *download[] = {"wget",
                            "-O",
                            "extensions.csv",
                            "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5",
                            NULL};

        execv("/usr/bin/wget", download);
        perror("execv");
        exit(EXIT_FAILURE);
    }
    else
    {
        int status;
        waitpid(tes, &status, 0);
        if (WEXITSTATUS(status) != 0)
        {
            fprintf(stderr, "Gagal mengunduh. 😭 \n");
            exit(EXIT_FAILURE);
        }
    }
}

void rot13decode(char *str)
{
    while (*str)
    {
        if ((*str >= 'A' && *str <= 'Z') || (*str >= 'a' && *str <= 'z'))
        {
            char base = (*str >= 'a' && *str <= 'z') ? 'a' : 'A';
            *str = ((*str - base + 13) % 26) + base;
        }
        str++;
    }
}

bool me_and_who(const char *str1, const char *str2)
{
    while (*str2)
    {
        if (*str2 == '*')
        {
            // Skip consecutive '*' characters
            while (*(str2 + 1) == '*')
                str2++;

            // Try matching the rest of str2 with str1 at different positions
            do
            {
                if (me_and_who(str1, str2 + 1))
                {
                    printf("matches");
                    return true;
                }
            } while (*str1++);

            return false;
        }
        else if (*str2 == '.' || *str1 == *str2)
        {
            str1++;
            str2++;
        }
        else
        {
            return false;
        }
    }

    return (*str1 == '\0'); // Check if the first string is also at its end
}

int foundVirus(char *filename, char *virusExtensions[], int numExtensions)
{
    char *ext = strrchr(filename, '.');
    if (ext != NULL)
    {
        ext++;
        for (int i = 0; i < numExtensions; i++)
        {
            if (me_and_who(filename, virusExtensions[i]))
            {
                printf("matches");
                return 1; // Ekstensi cocok dengan yang dianggap virus
            }
        }
    }
    return 0; // Ekstensi tidak ditemukan dalam daftar virus
}

int movefiles()
{
    FILE *file = fopen("extensions.csv", "r");
    if (file == NULL)
    {
        perror("Gagal membuka file extensions.csv");
        return EXIT_FAILURE;
    }

    char *virusExtensions[1000];
    int numExtensions = 0;

    char line[1000];
    while (fgets(line, sizeof(line), file))
    {
        line[strcspn(line, "\n")] = '\0';
        virusExtensions[numExtensions] = strdup(line);
        if (numExtensions > 7)
        {
            rot13decode(virusExtensions[numExtensions]);
            // printf("%s", virusExtensions[numExtensions]);
        }
        numExtensions++;
    }

    fclose(file);

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir("sisop_infected")) != NULL)
    {
        printf("open dir");
        while ((ent = readdir(dir)) != NULL)
        {
            if (ent->d_type == DT_REG)
            {
                if (foundVirus(ent->d_name, virusExtensions, numExtensions))
                {
                    char *sourcePath = (char *)malloc(strlen("sisop_infected/") + strlen(ent->d_name) + 1);
                    char *destPath = (char *)malloc(strlen("quarantine/") + strlen(ent->d_name) + 1);

                    sprintf(sourcePath, "sisop_infected/%s", ent->d_name);
                    sprintf(destPath, "quarantine/%s", ent->d_name);

                    if (rename(sourcePath, destPath) == 0)
                    {
                        printf("File %s dipindahkan ke quarantine\n", ent->d_name);
                        createlog(ent->d_name);
                    }
                    else
                    {
                        printf("Gagal memindahkan file %s ke quarantine\n", ent->d_name);
                    }
                    free(sourcePath);
                    free(destPath);
                }
            }
        }
        closedir(dir);
    }
    else
    {
        perror("Gagal membuka direktori sisop_infected");
        return EXIT_FAILURE;
    }

    for (int i = 0; i < numExtensions; i++)
    {
        free(virusExtensions[i]);
    }

    return 0;
}

void low(int sig)
{
    // createlog();
    printf("sek blom :c");
    printf("Caught signal %d\n", sig);
}

void medium(int sig)
{
    movefiles();
    printf("Caught signal %d\n", sig);
}

void hard(int sig)
{
    movefiles();
    printf("Caught signal %d\n", sig);
}

int bro(const char *itu)
{
    if (strcmp(itu, "low") == 0)
    {
        signal(SIGUSR1, low);
    }
    else if (strcmp(itu, "medium") == 0)
    {
        signal(SIGUSR2, medium);
    }
    else if (strcmp(itu, "hard") == 0)
    {
        signal(SIGRTMIN, hard);
    }
    else
    {
        printf("Invalid argument: %s\n", itu);
        return -1; // Return an error code for invalid input
    }
    return 0; // Return success
}

int main(int argc, char *argv[])
    {
        if (argc != 2)
        {
            exit(EXIT_FAILURE);
        }

        pid_t pid, sid; // variable to store pid

        pid = fork(); // store PID of Child Process

        // when fork fails
        if (pid < 0)
        {
            exit(EXIT_FAILURE);
        }

        // When fork succeeds
        // (variable pid is PID of child process)
        if (pid > 0)
        {
            exit(EXIT_SUCCESS);
        }

        umask(0); // to gain access to file created by daemon

        // give child process unique session id
        sid = setsid();
        if (sid < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (chdir("/") < 0)
        {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

    while (1)
{
    // donload();
    bro(argv[2]);

    sleep(1); // wait 1 sec before repeating the process
}

return 0;
}
