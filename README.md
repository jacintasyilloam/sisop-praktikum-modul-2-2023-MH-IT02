# Soal Shift Modul 2
## Sistem Operasi 2023

**Sisop girlbosses of IT02:**
| Nama | NRP |
| ---------------------- | ---------- |
| Azzahra Sekar Rahmadina | 5027221035 |
| Jacinta Syilloam | 5027221036 |
| Stephanie Hebrina Mabunbun Simatupang | 5027221069 |

## List of Contents
- [Soal 1](#soal-1) 
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Soal 4](#soal-4)

## Soal 1
This question explores the implementation of daemon process and argv to take a file path. 

### Explanation of code
***Main Function***
```bash
int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    exit(EXIT_FAILURE);
  }

  pid_t pid, sid; // variable to store pid

  pid = fork(); // store PID of Child Process

  // when fork fails
  if (pid < 0)
  {
    exit(EXIT_FAILURE);
  }

  // When fork succeeds
  // (variable pid is PID of child process)
  if (pid > 0)
  {
    exit(EXIT_SUCCESS);
  }

  umask(0); // to gain access to file created by daemon

  // give child process unique session id
  sid = setsid();
  if (sid < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (chdir("/") < 0)
  {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO); 
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1)
  {
    filelist(argv[1]); // run specified path
    sleep(30); // wait 30 secs before repeating the process   
  }

  return 0;
}
```
The main function consists of a daemon process template which allows this program to run in the background. To check if the process is actually runnin, ```ps aux``` is used.

Check if cleaner process is running:
![Screenshot_2023-10-08_014659](/uploads/63ff0e15f982c8c20bdcfce9d288c7ed/Screenshot_2023-10-08_014659.png)

```bash
  while (1)
  {
    filelist(argv[1]); // run specified path
    sleep(30); // wait 30 secs before repeating the process   
  }
```
```filelist(argv[1])``` takes the input of index 1 which is the directory path. 

#

***filelist Function***

```bash
void filelist(const char *path)
{
  printf("Run filelists..\n");
  DIR *directory;         // used to represent the opened directory
  struct dirent *txtfile; // holds info abt directory entry (file/subdir)

  directory = opendir(path); // open directory according to path

  if (directory != NULL)
  {

    if (readdir(directory) == NULL) // check if readdir works
    {
      printf("error readdir");
      return;
    }

    while (txtfile = readdir(directory)) // loop directory until theres no more entry
    {
      char file_path[1000]; // Store the full path to the file
      snprintf(file_path, sizeof(file_path), "%s/%s", path, txtfile->d_name);

      if (txtfile->d_type == DT_REG) // check if entry is a regular file
      {
        printf("found file\n");

        FILE *file = fopen(file_path, "r"); // open file

        if (file) 
        {
          char content[1000]; // array to store content
          while (fgets(content, sizeof(content), file))
          {
            if (strstr(content, "SUSPICIOUS") != NULL)
            {
              remove(file_path); // remove if theres sus
              createlog(file_path); // add log
              break;
            }
          }
        }
        fclose(file);
      }
      else if (txtfile->d_type == DT_DIR) // check if directory
      {
        printf("found a directory\n");
        if (strcmp(txtfile->d_name, ".") == 0 || strcmp(txtfile->d_name, "..") == 0) 
          continue; // skip current directory (.) and parent dir (..)

        filelist(file_path); // recursive
      }
    }
    closedir(directory);
  }
  else
  {
    printf("Error opendir");
    return;
  }
}
```
The function above works recursively by doing these steps: 
1. The function iterates within the directory of the inputted path
 ```while (txtfile = readdir(directory))```

2. *First condition*: When a regular file is found, it takes the file path and stores its content in an array.
```
char file_path[1000]; // Store the full path to the file
      snprintf(file_path, sizeof(file_path), "%s/%s", path, txtfile->d_name);
      ...
      char content[1000]; // array to store content
          while (fgets(content, sizeof(content), file))
```

3. If the content of the file happens to have the word "SUSPICIOUS", the file will be removed.
```
if (strstr(content, "SUSPICIOUS") != NULL)
            {
              remove(file_path); // remove if theres sus
```
4. A log about the file removal is then done by calling the createlog function.
```
              createlog(file_path); // add log
```
5. *Second condition*: When a directory is found, it takes the directory path.
```
      else if (txtfile->d_type == DT_DIR) // check if directory
```
6. The filelist function is then called again. This time the loop will iterate through the directory which was checked previously.
```
        filelist(file_path); // recursive
```
7. The function repeats until there are no more files with the word SUSPICIOUS in its content.

#

***createlog Function***
```bash
void createlog(const char *path)
{
  time_t t;
  struct tm *time_details;

  // get current time
  time(&t);
  time_details = localtime(&t);

  char formatted[50];

  // format the time string as requested
  strftime(formatted, sizeof(formatted), "[%Y-%m-%d %H:%M:%S]", time_details);

  FILE *log_file = fopen("/home/suguru/cleaner.log", "a"); // open log file
  if (log_file)
  {
    // write log message to log file
    fprintf(log_file, "%s '%s' has been removed.\n", formatted, path);
    fclose(log_file);
  }
}
```
To create log files, the function above utilizes the time.h library which provides information of the current time.

```  strftime(formatted, sizeof(formatted), "[%Y-%m-%d %H:%M:%S]", time_details);```

The current time is then formatted according to the desired output.

```fprintf(log_file, "%s '%s' has been removed.\n", formatted, path);```

By using fprintf, the log is printed inside the log file.

A peak to the cleaner.log file:
![Screenshot_2023-10-08_024030](/uploads/b5353117f89863f18d469f1436cfac74/Screenshot_2023-10-08_024030.png)

![Screenshot_2023-10-08_023951](/uploads/10da8b0c01d396931c6544b72cc15a24/Screenshot_2023-10-08_023951.png)

As seen from the two pictures above, the process iterates through all directory and the daemon process runs every 30 seconds.

End of explanation.

## Soal 2
In this number, we are asked to create a program called cavs.c

```
int main() {
    const char *folderName = "players";
    int status = mkdir(folderName, 0755);

    if (status == 0 || errno == EEXIST) {
        printf("Folder \"%s\" telah berhasil dibuat atau sudah ada.\n", folderName);
    } else {
        perror("Gagal membuat folder atau folder sudah ada");
        exit(EXIT_FAILURE);

```
This code creates a "players" directory with specific permissions and provides feedback on whether the directory was successfully created or already exists, or if there was a failure during the creation process.
```
void donload() {
    pid_t tes; //Nama proses id

    tes = fork();
    if (tes < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (tes == 0) {
        char *download[] = {"wget", 
        "-O",
        "players.zip", 
        "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", // ini make Google Drive Direct Link Generator buat tau linknya, nnti copy dr link yang ada di docs 😉
         NULL};

        execv("/usr/bin/wget", download); 
        perror("execv");
        exit(EXIT_FAILURE);
    } else {
        int status;
        waitpid(tes, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            fprintf(stderr, "Gagal mengunduh database pemain. 😭 \n");
            exit(EXIT_FAILURE);
        }
    }
}

```
- ```pid_t tes``` : This line declares a variable named tes of type pid_t. In the context of this function, tes will be used to store the Process ID (PID) of the child process that is created using fork().

- ```tes = fork() ``` : The fork() function is used to create a new child process. After this line, there are two processes: the parent process and the newly created child process. The value of tes will be different in each process: it will be non-zero in the parent process (indicating the PID of the child) and 0 in the child process.

- ```if (tes < 0) { ... }``` : This condition checks whether the fork() operation was successful. If tes is less than 0, it means that an error occurred during process creation. In this case, an error message is printed using perror("fork"), and the program exits with a failure status (EXIT_FAILURE) using exit(EXIT_FAILURE).

- ```if (tes == 0) { ... }: ``` This block of code is executed only by the child process. Inside this block, we configure the child process to run the wget command to download a file. 

| the download command | the function |
| ------ | ------ |
| wget        |   The command to run     |
| -O"       | An option that specifies the output file name       |
| players.zip       | The name of the output file, which will be "players.zip       |
| NULL     | The array is terminated with a NULL pointer       |


- ```execv("/usr/bin/wget", download) ```: The execv() function is used to replace the current program in the child process with the wget command specified in the download array. It effectively runs the wget command with the provided arguments. If this execution is successful, the child process will be replaced by the wget command, and the download will begin. If it fails, the child process exits with a failure status (EXIT_FAILURE) after printing an error message using perror("execv")

- ```else``` : After forking the child, the parent process waits for the child process to complete using the waitpid() function. It collects the exit status of the child process in the status variable. If the child process exits with a non-zero status, indicating an error during the download, an error message is printed, and the parent process exits with a failure status (EXIT_FAILURE).

**The uzip() function is responsible for extracting a ZIP file (specified by zipFileName) into a destination folder (specified by destFolder) using the unzip command-line tool :**
```
void uzip(const char *zipFileName, const char *destFolder) { // extract “players.zip” ==> unziepp
    pid_t zippie;
    int status;

    zippie = fork();
    if (zippie < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (zippie == 0) {
        execlp("unzip", "unzip", "-d", destFolder, zipFileName, NULL);
        perror("execv unzip");
        exit(EXIT_FAILURE);
    } else {
        waitpid(zippie, &status, 0);
    }
}
```

- ```pid_t zippie``` : This line declares a variable named zippie of type pid_t. It will be used to store the Process ID (PID) of the child process created by the fork() function.

- ```int status```: This line declares an integer variable named status. It will be used to store the exit status of the child process.

- ```zippie = fork()```: The fork() function is used to create a new child process. After this line, there are two processes: the parent process and the newly created child process. The value of zippie will be non-zero in the parent process (indicating the PID of the child) and 0 in the child process.

- ```if (zippie < 0) { ... }``` : This condition checks whether the fork() operation was successful. If zippie is less than 0, it means that an error occurred during process creation. In this case, an error message is printed using perror("fork"), and the program exits with a failure status (EXIT_FAILURE) using exit(EXIT_FAILURE)

- ```else if (zippie == 0) { ... }```: This block of code is executed only by the child process. Inside this block, the child process is configured to execute the unzip command 


| the command | the function |
| ------ | ------ |
| unzip       | The command to run (in this case, the unzip utility)       |
|  -d      |  An option that specifies the destination folder for extracting the contents of the ZIP file      |
| destFolder        |  The name of the destination folder where the ZIP contents will be extracted      |
| zipFileName       |  The name of the ZIP file to be extracted      |
| NULL       | The array is terminated with a NULL pointer       |

- ```execlp("unzip", "unzip", "-d", destFolder, zipFileName, NULL)``` : The execlp() function replaces the current program in the child process with the unzip command and its arguments provided in the array. If the execution is successful, the child process will become the unzip command, and it will extract the ZIP file contents into the specified destination folder. If it fails, an error message is printed using perror("execv unzip"), and the child process exits with a failure status (EXIT_FAILURE).

- ```else``` :This block is executed only by the parent process. After creating the child process, the parent process waits for the child process to complete using the waitpid() function. It collects the exit status of the child process in the status variable.
```
void hapus() { //hapus file zip tersebut agar tidak memenuhi komputer QQ
    pid_t hpus;
    int status;

    hpus = fork();
    if (hpus < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (hpus == 0) {
        execlp("rm", "rm", "players.zip", NULL); //buat remove
        perror("execv rm");
        exit(EXIT_FAILURE);
    } else {
        waitpid(hpus, &status, 0);
    }
}
```
This code creates a child process to run the "rm" command and delete the "players.zip" file, ensuring it doesn't occupy unnecessary space on the computer. The parent process waits for the child process to finish executing before continuing.

```
void filter_pemain(const char *folderName) { //ini untuk filter pemain dr luar Cavaliers,  biar kita bisa menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory
    DIR *dir = opendir(folderName);
    if (dir == NULL) {
        perror("Gagal membuka folder players"); 
        exit(EXIT_FAILURE);
    }
```

- ```void filter_pemain(const char *folderName)```: This line defines the filter_pemain() function, which takes a single argument: a pointer to a character string folderName. This function is intended to filter out players who are not from the "Cleveland Cavaliers" and remove them from the specified directory.

- ```if (dir == NULL) { ... }```: This condition checks if the opendir() function was successful in opening the directory. If dir is NULL, it indicates that there was an error in opening the directory, and the condition block is executed

- ```perror("Gagal membuka folder players")```: If the directory opening fails, the perror() function is used to print an error message with the description of the error to the standard error stream

- ```exit(EXIT_FAILURE)```: After printing the error message, the program exits with a failure status code (EXIT_FAILURE). This is done to terminate the program gracefully because the failure to open the directory suggests that further operations within the function might not be possible due to the missing directory

**This used to traverse a directory and delete files that do not contain the string "Cavaliers" in their filenames**
```
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            // ini buat memproses file (bukan folder)
            const char *filename = entry->d_name;

            // Cek apakah nama file mengandung "Cavaliers"
            if (strstr(filename, "Cavaliers") == NULL) {
                // nah klo gada kata 'Cavaliers' di foldernya itu kita delet
                char filePath[256];
                snprintf(filePath, sizeof(filePath), "%s/%s", folderName, filename);
                if (remove(filePath) != 0) {
                    perror("Gagal menghapus file");
                } else {
                    printf("Menghapus pemain: %s\n", filename); //hpus yg luar Cavaliers
                }
            }
        }
    }

    closedir(dir);
}
```

This code snippet iterates through files in a specified directory, checks if their filenames contain the string "Cavaliers," and if not, deletes them. It constructs the full file path, attempts deletion, and provides feedback on successful or failed deletions. Finally, it closes the directory after processing all files. 

```
void pindah_posisi() { // ini untuk mengirim player tadi ke direktori lain
    struct dirent *dir;
    DIR *d;
    d = opendir("players"); // direktori "players"
    if (d) {
        while ((dir = readdir(d)) != NULL) { // Loop
            if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                // agar tau kalau formatnya .png

                char filepath[512]; // menyimpan path file asal
                char position[10];  // menyimpan posisi pemain point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).
                snprintf(filepath, sizeof(filepath), "players/%s", dir->d_name); // path lengkap file asal
                sscanf(dir->d_name, "Cavaliers-%2s", position); // posisi pemain dari nama file (point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C))

                char position_folder[32]; // savee path folder berdasarkan posisi
                snprintf(position_folder, sizeof(position_folder), "players/%s", position); // sv path folder berdasarkan posisi

                struct stat st = {0};
                if (stat(position_folder, &st) == -1) {
                    // memeriksa jika folder berdasarkan posisi sudah ada, jika tidak, maka buat folder baru
                    mkdir(position_folder, 0770);
                }

                char new_filepath[512]; // save file akhir
                snprintf(new_filepath, sizeof(new_filepath), "%s/%s", position_folder, dir->d_name); // save path file tujuan

                // mindahin file ke folder berdasarkan posisi pemain
                rename(filepath, new_filepath);
            }
        }
        closedir(d);
    }
}
 
```
This code defines a function ```pindah_posisi()``` that is responsible for moving player image files from one directory to another based on their positions (point guard, shooting guard, small forward, power forward, or center). It does so by iterating through files in the "players" directory, identifying files with filenames containing "Cavaliers" and ending with ".png" to ensure they are PNG image files. It extracts the player's position from the filename, creates destination folders for each position if they don't already exist, and then moves the files to their respective position folders. This function uses file manipulation functions like ```rename``` and directory creation functions like ```mkdir``` to achieve this file organization task.

```
void kategorisasi_posisi(const char *folderName) {
    int pg_count = 0;
    int sg_count = 0;
    int sf_count = 0;
    int pf_count = 0;
    int c_count = 0;

    struct dirent *dir;
    DIR *d;
    d = opendir(folderName);

    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                char position[10];
                sscanf(dir->d_name, "Cavaliers-%2s", position);

                if (strcmp(position, "PG") == 0) {
                    pg_count++;
                } else if (strcmp(position, "SG") == 0) {
                    sg_count++;
                } else if (strcmp(position, "SF") == 0) {
                    sf_count++;
                } else if (strcmp(position, "PF") == 0) {
                    pf_count++;
                } else if (strcmp(position, "C-") == 0) {
                    c_count++;
                }
            }
        }
        closedir(d);

        // Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
        FILE *formasi_file = fopen("Formasi.txt", "w");
        if (formasi_file != NULL) {
            fprintf(formasi_file, "PG: %d\n", pg_count);
            fprintf(formasi_file, "SG: %d\n", sg_count);
            fprintf(formasi_file, "SF: %d\n", sf_count);
            fprintf(formasi_file, "PF: %d\n", pf_count);
            fprintf(formasi_file, "C: %d\n", c_count);
            fclose(formasi_file);
        } else {
            perror("Gagal membuka Formasi.txt 😭");
        }
    }
}
```
This code defines a function `kategorisasi_posisi` that categorizes and counts player image files based on their positions (PG, SG, SF, PF, or C) found in the specified directory `folderName`. It uses a set of counters (`pg_count`, `sg_count`, `sf_count`, `pf_count`, and `c_count`) to keep track of the number of files for each position. The code iterates through files in the directory, extracts the position information from filenames, and increments the corresponding counter. After processing all the files, it creates or overwrites a "Formasi.txt" file to store the categorized counts. Each position's count is written to this text file, and if successful, it's closed. If there's an issue with file creation, an error message is displayed. This code essentially counts and records the number of player images for each position in a text file.

**For the last 2 points, the coding application is the same:**

```
void majang_foto1(const char *folderName) {
    char sourcePath[256];
    char destinationPath[256];

    snprintf(sourcePath, sizeof(sourcePath), "%sPG/Cavaliers-PG-Kyrie-Irving.png", folderName);
    snprintf(destinationPath, sizeof(destinationPath), "clutch/Cavaliers-PG-Kyrie-Irving.png"); // "clutch" folder in the current directory

    int result = rename(sourcePath, destinationPath);

    if (result == 0) {
        printf("Pemindahan Kyrie Irving ke folder 'clutch' berhasil.\n");
    } else {
        perror("Gagal memindahkan Kyrie Irving ke folder 'clutch'");
    }
}

```
This code defines a function majang_foto1 that moves a specific image file (Kyrie Irving's image with the "Cavaliers-PG" position) from one location to another. 

It declares two character arrays, `sourcePath` and `destinationPath`, to store the source and destination file paths, respectively.
It uses `snprintf` to construct the source and destination file paths based on the provided folderName and fixed filename information.
It attempts to `rename` (move) the file from the `sourcePath` to the `destinationPath` using the rename function.
If the renaming operation is successful `(result == 0)`, it prints a success message indicating that Kyrie Irving's image has been moved to the "clutch" folder.

That's all for num 2, signing off 🥲

## Soal 3
This program serves to assist the artist Albedo in collecting reference images for his artwork from the internet. It does so by periodically creating timestamped folders, downloading square images, and organizing them. When a folder holds 15 images, the program compresses it into a ZIP file and removes the original. It also generates a "killer" program for immediate or delayed program termination, depending on the chosen mode (MODE_A or MODE_B).

**A**

`Creates timed folders with a C program that generates folders per 30 seconds, named with timestamps format [YYYY-MM-dd_HH:mm:ss].`

```bash
// Function to create a folder
void createFolder() {
   char timestamp[20];
   char folder[30];
   time_t t = time(NULL);
   struct tm* tm_now = localtime(&t);
   strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", tm_now);
   snprintf(folder, sizeof(folder), "%s", timestamp);

   pid_t cid;
    cid = fork();

    if (cid < 0) {
        exit(EXIT_FAILURE);
    }

    if (cid == 0) {
        if (fork() == 0) {
            char *mkdir_argv[] = {"mkdir", "-p", folder, NULL};
            execv("/bin/mkdir", mkdir_argv);
            perror("execv");
            exit(EXIT_FAILURE);
        } else {
            int stat;
            while (wait(&stat) > 0);

            downloadImages(folder);
            zipAndDelete(folder);
        }
    }
}
```
This code creates a timestamp-based folder that generates a timestamp string in the format `YYYY-MM-DD_HH:MM:SS` and forks a child process to create that folder. In the child process, it uses the `mkdir` command to create a directory with the generated timestamp as its name. After creating the directory, it waits for the `mkdir` process to finish, and then it calls the `downloadImages` and `zipAndDelete` functions to perform additional operations on the newly created folder.

**B**

`Fills each folder with 15 square images downloaded from https://source.unsplash.com/{widthxheight}, with image dimensions determined by (t%1000)+50 pixels. Images are named with timestamps.`

```bash
// Function to download images into a folder
void downloadImages(const char* folder) {
    if (chdir(folder) == -1) {
        perror("chdir");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 15; i++) {
       char timestamp[20];
       char url[100];
       char filename[50];
       time_t t = time(NULL);
       int size = (int)t % 1000 + 50;
       strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", localtime(&t));
       snprintf(url, sizeof(url), "https://source.unsplash.com/%dx%d", size, size);
       snprintf(filename, sizeof(filename), "%s.jpg", timestamp);

       pid_t pid = fork();
       if (pid == 0) {
           char *args[] = {"wget", url, "-qO", filename, NULL};
           execvp("/usr/bin/wget", args);
           perror("execvp");
           exit(EXIT_FAILURE);
       } else {
           sleep(5);
       }
   }
   zipAndDelete(folder);
}
```
This code downloads 15 images with random sizes and filenames to the specified folder using the `wget` command. The code forks a child process for each image download, and each child process executes `wget` to fetch an image, waits for 5 seconds between downloads, and once all images are downloaded, it calls the `zipAndDelete` function to compress the folder's contents and clean up.

**C**

` Maintains organization by compressing a folder, which contains 15 images, into a ZIP file and deleting the original folder, leaving only the .zip file.`

```bash
// Function to zip folder then deletes it
void zipAndDelete(const char* folder) {
    chdir("..");

    char zip[100];
    snprintf(zip, sizeof(zip), "%.90s.zip", folder);

    pid_t pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        char *args[] = {"zip", "-qrm", zip, folder, NULL};
        execv("/usr/bin/zip", args);
        perror("execv");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }
}
```
This code performs two main tasks. First, it changes the current directory to the parent directory using `chdir("..")`. Then, it creates a ZIP file named after the folder and compresses the folder's contents. The code forks a child process to execute the 'zip' command, and once the zipping is complete, it waits for the child process to finish.

**D & E**

`Ensures control by generating a "killer" program to terminate all program operations, which can be executed and then self-deleted. Operates in two modes: MODE_A and MODE_B. In MODE_A, running the "killer" program stops all operations immediately. In MODE_B, the program stops but allows ongoing folder processes to finish (i.e., filling, zipping, and deleting)`

```bash
// Function to generate a "killer" program
void generateKiller(const char *mode) {
   // Create the "killer" code in a "killer.sh" file
   FILE* kp = fopen("killer.sh", "w");
   if (kp == NULL) {
       perror("fopen");
       exit(EXIT_FAILURE);
   }

   fprintf(kp, "#!/bin/bash\n");
   if (strcmp("-a", mode) == 0) {
   // Mode A: Terminate the program using killall and remove "killer.sh"
       fprintf(kp, "killall -9 lukisan\nrm $0\n");
   } else if (strcmp("-b", mode) == 0) {
   // Mode B: Terminate the program but allow processes in folders to finish
       fprintf(kp, "kill_parent() {\n");
       fprintf(kp, "local parent_pid=$1\n");
       fprintf(kp, "kill $parent_pid\n");
       fprintf(kp, "}\n");
       fprintf(kp, "kill_parent $(pgrep lukisan)\nrm $0\n");
   }

   fclose(kp);

   pid_t pid = fork();
   if (pid < 0){
       perror("fork");
       exit(EXIT_FAILURE);
   }
   if (pid == 0){
       execlp("/bin/chmod", "chmod", "+x", "killer.sh", NULL);
       perror("execlp");
       exit(EXIT_FAILURE);
   }
   while(wait(NULL) != pid);
}
```
This function, creates a script named `killer.sh` to terminate the "lukisan" process based on the provided 'mode' argument. If 'mode' is "-a," it kills all instances of "lukisan," and if 'mode' is "-b," it kills the parent process. 
After creating the script, it makes it executable using `chmod` and waits for the `chmod` process to complete before returning. 
The purpose of this function is to provide a way to terminate the "lukisan" process, either by killing all instances or specifically the parent process, based on the chosen mode.

**Main Function**
```bash
int main(int argc, char *argv[]) {
   if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
       fprintf(stderr, "Usage: %s <-a/-b>\n", argv[0]);
       exit(EXIT_FAILURE);
   }

   generateKiller(argv[1]); // Generate the "killer" program based on the specified mode

   pid_t pid = fork(), sid;
   if (pid < 0) {
       exit(EXIT_FAILURE);
   }
   if (pid > 0) {
       exit(EXIT_SUCCESS);
   }

   umask(0);

   sid = setsid();
   if (sid < 0) {
       exit(EXIT_FAILURE);
   }

   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);

    //main loop of the program
   while (1) { 
       createFolder();
       sleep(30); // Sleep for 30 seconds before creating the next folder
   }

   return 0;
}
```
This program verifies it's called with either "-a" or "-b" as an argument and generates a "killer.sh" script accordingly. It then creates a daemon that periodically generates folders every 30 seconds using the `createFolder` function. The daemon runs indefinitely while the main process exits successfully.

In summary, this program sets up a background task that creates folders at regular intervals and can be stopped using the generated "killer.sh" script with the chosen mode ("-a" or "-b").

### Output
***Compile Program and Run Program Mode A***
![compile_.lukisan_-a](/uploads/b167f5fd1318e232a63008533202f0d6/compile_.lukisan_-a.png)
![stlh_lukisan_-a](/uploads/2e22d732e36717b3e6b1b1f6b5ed993e/stlh_lukisan_-a.png)
***Kill Program***
![di_kill_-a](/uploads/833b4d04bbf6bcb777de73e9e6b76dc3/di_kill_-a.png)
![setelah_di_kill_-a](/uploads/8531477382198e22a09afcf3baf3facb/setelah_di_kill_-a.png)
***Isi Folder***
![isi_folder](/uploads/21e54b23b92954b19cd0f65c49737c1a/isi_folder.png)

***Run Program Mode B***
![.lukisan_b](/uploads/f81218bda768c7297d5ed0fdb99d20cf/.lukisan_b.png)
![setelah_-b](/uploads/66420a7d45228b71ea1880c91fe4129a/setelah_-b.png)
***Kill Program***
![kill_b](/uploads/b10dabe0a68bfb174f5efe20aca1750b/kill_b.png)![stlh_kill_-b](/uploads/cfb7fd3c98cbd6b0940633d59a0e9fb6/stlh_kill_-b.png)
That's all for num 3, signing off now 😉👋🏻🐱

## Soal 4
This question delves in signal handling in c and rot13 encrypted string.

### Explanation of code 
***Main Function***
```bash
int main(int argc, char *argv[])
    {
        if (argc != 2)
        {
            exit(EXIT_FAILURE);
        }

        pid_t pid, sid; // variable to store pid

        pid = fork(); // store PID of Child Process

        // when fork fails
        if (pid < 0)
        {
            exit(EXIT_FAILURE);
        }

        // When fork succeeds
        // (variable pid is PID of child process)
        if (pid > 0)
        {
            exit(EXIT_SUCCESS);
        }

        umask(0); // to gain access to file created by daemon

        // give child process unique session id
        sid = setsid();
        if (sid < 0)
        {
            exit(EXIT_FAILURE);
        }

        if (chdir("/") < 0)
        {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

    while (1)
{
    donload();
    bro(argv[2]);

    sleep(1); // wait 1 sec before repeating the process
}

return 0;
}
```
The main function consists of a daemon process template which allows this program to run in the background. To check if the process is actually running, ```ps aux``` is used.

```bash
    while (1)
{
    donload();
    bro(argv[2]);

    sleep(1); // wait 1 sec before repeating the process
}
```
As the daemon process runs, it calls the donload function which downloads a csv file. ```bro(argv[2])``` takes index 2 of user input which describes the level.

#
***donload Function***
```bash
void donload()
{
    pid_t tes;

    tes = fork();
    if (tes < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (tes == 0)
    {
        char *download[] = {"wget",
                            "-O",
                            "extensions.csv",
                            "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5",
                            NULL};

        execv("/usr/bin/wget", download);
        perror("execv");
        exit(EXIT_FAILURE);
    }
    else
    {
        int status;
        waitpid(tes, &status, 0);
        if (WEXITSTATUS(status) != 0)
        {
            fprintf(stderr, "Gagal mengunduh. 😭 \n");
            exit(EXIT_FAILURE);
        }
    }
}
```
- The donload() function is designed to download a file from the internet using the wget command through a child process and wait for the download to complete. 


- pid_t tes: This is a declaration of a variable of type pid_t that will be used to store the Process ID (PID)

- tes = fork();: The fork() function is used to start a copy of the presently active process as a child process. The tes variable holds the fork() output. The parent process and the child process are now both active after using fork().

- if (tes < 0): This condition determines whether or not the child process creation was successful. The programme terminates with an error status of (EXIT_FAILURE) if the value of tes is less than 0. This indicates that there was a problem when the child process was created.

- If (tes == 0), the child process will carry out this section. The command and arguments to be used when calling execv() are defined in an array named download that is defined inside the child process. In this instance, a wget command is being used to get the file "extensions.csv" from a particular URL. The child process will execute the wget command if the execv() function is successful. If it is unsuccessful, an error message is printed and the child process terminates with the error status (EXIT_FAILURE).
- else: The parent process will carry out this step. The parent process uses waitpid() to wait for the child process to finish after it has been created. It collects the child process's exit status and determines whether it exited with a non-zero status. If the child process ran into a problem while downloading (such as a network problem), it prints an error message. Otherwise, it means the download was successful.

- the donload() function forks a child process to execute the wget command for downloading a file. It checks for errors in the child process and waits for it to complete before returning to the parent process.

#
***movefiles Function*** 
```bash
int movefiles()
{
    FILE *file = fopen("extensions.csv", "r");
    if (file == NULL)
    {
        perror("Gagal membuka file extensions.csv");
        return EXIT_FAILURE;
    }

    char *virusExtensions[1000];
    int numExtensions = 0;

    char line[1000];
    while (fgets(line, sizeof(line), file))
    {
        line[strcspn(line, "\n")] = '\0';
        virusExtensions[numExtensions] = strdup(line);
        if (numExtensions > 7)
        {
            rot13decode(virusExtensions[numExtensions]);
            // printf("%s", virusExtensions[numExtensions]);
        }
        numExtensions++;
    }

    fclose(file);

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir("sisop_infected")) != NULL)
    {
        printf("open dir");
        while ((ent = readdir(dir)) != NULL)
        {
            if (ent->d_type == DT_REG)
            {
                if (foundVirus(ent->d_name, virusExtensions, numExtensions))
                {
                    char *sourcePath = (char *)malloc(strlen("sisop_infected/") + strlen(ent->d_name) + 1);
                    char *destPath = (char *)malloc(strlen("quarantine/") + strlen(ent->d_name) + 1);

                    sprintf(sourcePath, "sisop_infected/%s", ent->d_name);
                    sprintf(destPath, "quarantine/%s", ent->d_name);

                    if (rename(sourcePath, destPath) == 0)
                    {
                        printf("File %s dipindahkan ke quarantine\n", ent->d_name);
                        createlog(ent->d_name);
                    }
                    else
                    {
                        printf("Gagal memindahkan file %s ke quarantine\n", ent->d_name);
                    }
                    free(sourcePath);
                    free(destPath);
                }
            }
        }
        closedir(dir);
    }
    else
    {
        perror("Gagal membuka direktori sisop_infected");
        return EXIT_FAILURE;
    }

    for (int i = 0; i < numExtensions; i++)
    {
        free(virusExtensions[i]);
    }

    return 0;
}
```


The function above is revised. Visit revision at the end of soal 4 explanation.
#
***rot13decode Function***
```bash
void rot13decode(char *str)
{
    while (*str)
    {
        if ((*str >= 'A' && *str <= 'Z') || (*str >= 'a' && *str <= 'z'))
        {
            char base = (*str >= 'a' && *str <= 'z') ? 'a' : 'A';
            *str = ((*str - base + 13) % 26) + base;
        }
        str++;
    }
}
```
ROT13 is short for rotate by 13 places. It is a letter subsitution cipher that shifts letters by 13 places.

How the function above decodes rot13:
1. First it iterates through each character of the string using ```while (*str)```.
2. Check if it's a character.
```
        if ((*str >= 'A' && *str <= 'Z') || (*str >= 'a' && *str <= 'z'))
```
3. Set case type as the base.
```
            char base = (*str >= 'a' && *str <= 'z') ? 'a' : 'A';
```
4. Decode rot13 according to the formula.
```
        if ((*str >= 'A' && *str <= 'Z') || (*str >= 'a' && *str <= 'z'))
        {
            char base = (*str >= 'a' && *str <= 'z') ? 'a' : 'A';
            *str = ((*str - base + 13) % 26) + base;
        }
```

With this function, the extensions are now decoded like below:
![Screenshot_2023-10-07_210851](/uploads/f2ad546f1c6a7e19b08a6b0409826350/Screenshot_2023-10-07_210851.png)


#
***foundVirus Function***
```
int foundVirus(char *filename, char *virusExtensions[], int numExtensions)
{
    char *ext = strrchr(filename, '.');
    if (ext != NULL)
    {
        ext++;
        for (int i = 0; i < numExtensions; i++)
        {
            if (me_and_who(filename, virusExtensions[i]))
            {
                printf("matches");
                return 1; // Ekstensi cocok dengan yang dianggap virus
            }
        }
    }
    return 0; // Ekstensi tidak ditemukan dalam daftar virus
}
```
How this function works:
1. First it extracts the extension of the file by observing the last occurrence of a dot (.) using strrchr. ```ext++``` is then used to skip the dot.
```
    char *ext = strrchr(filename, '.');
    ...
      ext++;
```

2. Loop through list of extension.
```
        for (int i = 0; i < numExtensions; i++)
```

3. Calls the me_and_who function which compares the extension of the file with extensions present in the list.
```
            if (me_and_who(filename, virusExtensions[i]))
```
4. Return 1 (true) if a match is found.

#
***me_and_who Function***
```
bool me_and_who(const char *str1, const char *str2)
{
    while (*str2)
    {
        if (*str2 == '*')
        {
            // Skip consecutive '*' characters
            while (*(str2 + 1) == '*')
                str2++;

            // Try matching the rest of str2 with str1 at different positions
            do
            {
                if (me_and_who(str1, str2 + 1))
                {
                    printf("matches");
                    return true;
                }
            } while (*str1++);

            return false;
        }
        else if (*str2 == '.' || *str1 == *str2)
        {
            str1++;
            str2++;
        }
        else
        {
            return false;
        }
    }

    return (*str1 == '\0'); // Check if the first string is also at its end
}
```
The function above compares two strings and checks if the characters matches.

The function itself works as follows: 
1. Take two strings using a pointer array.
```
bool me_and_who(const char *str1, const char *str2)
```
2. Loop through the second string.
```
    while (*str2)
```
3. The loop checks three condition
- If the first character in \*str2 is a wildcard (\*): Skip '*' and continue with recurison
```
        if (*str2 == '*')
        {
            // Skip consecutive '*' characters
            while (*(str2 + 1) == '*')
                str2++;

            // Try matching the rest of str2 with str1 at different positions
            do
            {
                if (me_and_who(str1, str2 + 1))

```
- If the first character in \*str2 is a dot (.) or matches the current character in \*str1: Increment both strings
```
        else if (*str2 == '.' || *str1 == *str2)
        {
            str1++;
            str2++;
        }
```
- If the two above conditions are not met, return false.
```
        else
        {
            return false;
        }
```

4. Check if string is at its end
```
    return (*str1 == '\0'); // Check if the first string is also at its end
```
#
***createlog Function***
```
void createlog(const char *file)
{
    time_t t;
    struct tm *time_details;

    time(&t);
    time_details = localtime(&t);

    char formatted[50];
    strftime(formatted, sizeof(formatted), "%Y-%m-%d %H:%M:%S", time_details);

    FILE *log_file = fopen("/home/suguru/virus.log", "a");
    if (log_file)
    {
        printf("log is created");
        fprintf(log_file, "[%s][%s] - %s - Moved to quarantine\n", convertname(file), formatted, file);
        fclose(log_file);
    }
}
```
To create log files, the function above takes in the file name, then utilizes the time.h library which provides information of the current time.

```    
strftime(formatted, sizeof(formatted), "%Y-%m-%d %H:%M:%S", time_details);
```

The current time is then formatted according to the desired output. It also calls the convertname() function which returns the name of the file owner.

```
fprintf(log_file, "[%s][%s] - %s - Moved to quarantine\n", convertname(file), formatted, file);
```

By using fprintf, the log is printed inside the log file.

A peak to the virus.log file:
![Screenshot_2023-10-08_164925](/uploads/f8d048e19e0422955bbec1ac0e540eab/Screenshot_2023-10-08_164925.png)

#
***convertname Function***
```
const char *convertname(const char *filename)
{
    struct stat file_info;

    if (stat(filename, &file_info) == 0)
    {
        // Get the username from the user ID
        struct passwd *owner_info = getpwuid(file_info.st_uid);
        if (owner_info != NULL)
        {
            printf("Owner of %s is %s\n", filename, owner_info->pw_name);
            return owner_info->pw_name;
        }
        else
        {
            perror("getpwuid");
        }
    }
    else
    {
        perror("stat");
    }

    return NULL;
}
```
The function above is revised, please visit revision at the end of explanation.


#
***bro Function***
```
int bro(const char *itu)
{
    if (strcmp(itu, "low") == 0)
    {
        signal(SIGUSR1, low);
    }
    else if (strcmp(itu, "medium") == 0)
    {
        signal(SIGUSR2, medium);
    }
    else if (strcmp(itu, "hard") == 0)
    {
        signal(SIGRTMIN, hard);
    }
    else
    {
        printf("Invalid argument: %s\n", itu);
        return -1; // Return an error code for invalid input
    }
    return 0; // Return success
}
```
The function above takes the level from user input and uses strcmp to compare it to the available levels (low, medium, or hard). 

It then triggers the signal handler according to the question.

#
***Signal Handlers***
```
void low(int sig)
{
    // createlog();
    printf("sek blom :c");
    printf("Caught signal %d\n", sig);
}

void medium(int sig)
{
    movefiles();
    printf("Caught signal %d\n", sig);
}

void hard(int sig)
{
    movefiles();
    printf("Caught signal %d\n", sig);
}
```
The program will then operate according to the signal that was called.

#
***Complete revised code for antivirus.c***
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <signal.h>
#include <pwd.h>
#include <time.h>
#include <sys/wait.h>
#include <stdbool.h>

int level;

const char *convertname(const char *filename)
{
    struct stat st;
    struct passwd *owner_info;

    if (lstat(filename, &st) < 0)
    {
        printf("Couldn't open file.\n");
        return NULL;
    }

    if ((owner_info = getpwuid(st.st_uid)) == NULL)
    {
        printf("Unable to retrieve owner information.\n");
        return NULL;
    }

    return owner_info->pw_name;
}

void createlog(const char *file, int level)
{
    time_t t;
    struct tm *time_details;

    time(&t);
    time_details = localtime(&t);

    char formatted[50];
    strftime(formatted, sizeof(formatted), "%Y-%m-%d %H:%M:%S", time_details);

    FILE *log_file = fopen("/home/suguru/virus.log", "a");
    if (log_file)
    {
        const char *ownerName = convertname(file);
        printf("owner name: %s", ownerName);

        printf("log is created");
        if (level == 1)
        {
            fprintf(log_file, "[%s][%s] - %s - Activity log recorded in virus.log\n", ownerName, formatted, file);
        }
        else if (level == 2)
        {
            fprintf(log_file, "[%s][%s] - %s - Moved to quarantine\n", ownerName, formatted, file);
        }
        else if (level == 3)
        {
            fprintf(log_file, "[%s][%s] - %s - File has been deleted\n", ownerName, formatted, file);
        }
        fclose(log_file);
    }
}

void donload()
{
    pid_t tes;

    tes = fork();
    if (tes < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (tes == 0)
    {
        char *download[] = {"wget",
                            "-O",
                            "extensions.csv",
                            "https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5",
                            NULL};

        execv("/usr/bin/wget", download);
        perror("execv");
        exit(EXIT_FAILURE);
    }
    else
    {
        int status;
        waitpid(tes, &status, 0);
        if (WEXITSTATUS(status) != 0)
        {
            fprintf(stderr, "Gagal mengunduh. 😭 \n");
            exit(EXIT_FAILURE);
        }
    }
}

void rot13decode(char *str)
{
    while (*str)
    {
        if ((*str >= 'A' && *str <= 'Z') || (*str >= 'a' && *str <= 'z'))
        {
            char base = (*str >= 'a' && *str <= 'z') ? 'a' : 'A';
            *str = ((*str - base + 13) % 26) + base;
        }
        str++;
    }
}

bool me_and_who(const char *str1, const char *str2)
{
    while (*str2)
    {
        if (*str2 == '*')
        {
            // Skip consecutive '*' characters
            while (*(str2 + 1) == '*')
                str2++;

            // Try matching the rest of str2 with str1 at different positions
            do
            {
                if (me_and_who(str1, str2 + 1))
                {
                    printf("matches me n who\n");
                    return true;
                }
            } while (*str1++);

            return false;
        }
        else if (*str2 == '.' || *str1 == *str2)
        {
            str1++;
            str2++;
        }
        else
        {
            return false;
        }
    }

    return (*str1 == '\0'); // Check if the first string is also at its end
}

int foundVirus(char *filename, char *virusExtensions[], int numExtensions)
{
    char *ext = strrchr(filename, '.');
    if (ext != NULL)
    {
        ext++;
        for (int i = 0; i < numExtensions; i++)
        {
            if (me_and_who(filename, virusExtensions[i]))
            {
                printf("matches found virus\n");
                return 1; // Ekstensi cocok dengan yang dianggap virus
            }
        }
    }
    return 0; // Ekstensi tidak ditemukan dalam daftar virus
}

int movefiles()
{
    // opening csv file
    FILE *file = fopen("extensions.csv", "r");
    if (file == NULL)
    {
        perror("Gagal membuka file extensions.csv");
        return EXIT_FAILURE;
    }

    char *virusExtensions[1000];
    int numExtensions = 0;

    // decode
    char line[1000];
    while (fgets(line, sizeof(line), file))
    {
        line[strcspn(line, "\n")] = '\0';
        virusExtensions[numExtensions] = strdup(line);
        if (numExtensions > 7)
        {
            rot13decode(virusExtensions[numExtensions]);
            // printf("%s", virusExtensions[numExtensions]);
        }
        numExtensions++;
    }

    fclose(file);

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir("sisop_infected")) != NULL)
    {
        printf("open dir sisop infected succeeds\n");
        while ((ent = readdir(dir)) != NULL)
        {
            if (ent->d_type == DT_REG)
            {
                if (foundVirus(ent->d_name, virusExtensions, numExtensions))
                {
                    printf("found virus return true\n");
                    printf("current level is %d\n", level);

                    char *sourcePath = (char *)malloc(strlen("sisop_infected/") + strlen(ent->d_name) + 1);
                    char *destPath = (char *)malloc(strlen("quarantine/") + strlen(ent->d_name) + 1);

                    sprintf(sourcePath, "sisop_infected/%s", ent->d_name);
                    sprintf(destPath, "quarantine/%s", ent->d_name);

                    printf("sourcePath: %s\n destpath: %s", sourcePath, destPath);

                    if (level == 1)
                    {
                        createlog(ent->d_name, level);
                    }
                    else if (level == 2)
                    {
                        level == 2;
                        if (rename(sourcePath, destPath) == 0)
                        {
                            printf("File %s dipindahkan ke quarantine\n", ent->d_name);
                            createlog(ent->d_name, level);
                        }
                        else
                        {
                            printf("Gagal memindahkan file %s ke quarantine\n", ent->d_name);
                        }

                        free(sourcePath);
                        free(destPath);
                    }
                    else if (level == 3)
                    {
                        level == 3;
                        createlog(ent->d_name, level);
                        remove(sourcePath);
                    }
                }
            }
        }
        closedir(dir);
    }
    else
    {
        perror("Gagal membuka direktori sisop_infected");
        return EXIT_FAILURE;
    }

    for (int i = 0; i < numExtensions; i++)
    {
        free(virusExtensions[i]);
    }

    return 0;
}

void signal_handler(int itu)
{
    if (itu == SIGUSR1)
    {
        printf("level changed to low");
        level = 1;
    }
    else if (itu == SIGUSR2)
    {
        printf("level changed to medium");
        level = 2;
    }
    else if (itu == SIGRTMIN)
    {
        printf("level changed to hard");
        level = 3;
    }
    else if (itu == SIGINT)
    {
        printf("i quit bro dis aint wat i wana do in life 💔");
        break;
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        exit(EXIT_FAILURE);
    }

    // turn levels into int
    if (strcmp(argv[2], "low") == 0)
    {
        level = 1;
    }
    if (strcmp(argv[2], "medium") == 0)
    {
        level = 2;
    }
    if (strcmp(argv[2], "hard") == 0)
    {
        level = 3;
    }

    // signal
    signal(SIGUSR1, signal_handler);
    signal(SIGUSR2, signal_handler);
    signal(SIGRTMIN, signal_handler);
    signal(SIGINT, signal_handler);

    pid_t pid, sid; // variable to store pid

    pid = fork(); // store PID of Child Process

    // when fork fails
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    // When fork succeeds
    // (variable pid is PID of child process)
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0); // to gain access to file created by daemon

    // give child process unique session id
    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        // donload();
        printf("jalan gk\n");
        printf("ada %s\n", argv[1]);

        movefiles();
        sleep(1); // wait 1 sec before repeating the process
    }

    return 0;
}
```

- Revisions done in movefiles function: added level conditions 
- Added feature to stop the program: user can send SIGINT signal
- File owner name revised


End of explanation.
#

**This marks the end of our lapres.**

Have a great day sir azril, IT02 SIGNING OFF‼️
