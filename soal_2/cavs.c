#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/wait.h>
#include <errno.h>

//shms
//ini untuk soal B dimana kita mengunduh file yang berisikan database para pemain bola
void donload() {
    pid_t tes; //Nama proses id

    tes = fork();
    if (tes < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (tes == 0) {
        char *download[] = {"wget", 
        "-O",
        "players.zip", 
        "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", // ini make Google Drive Direct Link Generator buat tau linknya, nnti copy dr link yang ada di docs 😉
         NULL};

        execv("/usr/bin/wget", download); 
        perror("execv");
        exit(EXIT_FAILURE);
    } else {
        int status;
        waitpid(tes, &status, 0);
        if (WEXITSTATUS(status) != 0) {
            fprintf(stderr, "Gagal mengunduh database pemain. 😭 \n");
            exit(EXIT_FAILURE);
        }
    }
}


void uzip(const char *zipFileName, const char *destFolder) { // extract “players.zip” ==> unziepp
    pid_t zippie;
    int status;

    zippie = fork();
    if (zippie < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (zippie == 0) {
        execlp("unzip", "unzip", "-d", destFolder, zipFileName, NULL);
        perror("execv unzip");
        exit(EXIT_FAILURE);
    } else {
        waitpid(zippie, &status, 0);
    }
}


void hapus() { //hapus file zip tersebut agar tidak memenuhi komputer QQ
    pid_t hpus;
    int status;

    hpus = fork();
    if (hpus < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (hpus == 0) {
        execlp("rm", "rm", "players.zip", NULL); //buat remove
        perror("execv rm");
        exit(EXIT_FAILURE);
    } else {
        waitpid(hpus, &status, 0);
    }
}

void filter_pemain(const char *folderName) { //ini untuk filter pemain dr luar Cavaliers,  biar kita bisa menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory
    DIR *dir = opendir(folderName);
    if (dir == NULL) {
        perror("Gagal membuka folder players"); 
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            // ini buat memproses file (bukan folder)
            const char *filename = entry->d_name;

            // Cek apakah nama file mengandung "Cavaliers"
            if (strstr(filename, "Cavaliers") == NULL) {
                // nah klo gada kata 'Cavaliers' di foldernya itu kita delet
                char filePath[256];
                snprintf(filePath, sizeof(filePath), "%s/%s", folderName, filename);
                if (remove(filePath) != 0) {
                    perror("Gagal menghapus file");
                } else {
                    printf("Menghapus pemain: %s\n", filename); //hpus yg luar Cavaliers
                }
            }
        }
    }

    closedir(dir);
}

void pindah_posisi() { // ini untuk mengirim player tadi ke direktori lain
    struct dirent *dir;
    DIR *d;
    d = opendir("players"); // direktori "players"
    if (d) {
        while ((dir = readdir(d)) != NULL) { // Loop
            if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                // agar tau kalau formatnya .png

                char filepath[512]; // menyimpan path file asal
                char position[10];  // menyimpan posisi pemain point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).
                snprintf(filepath, sizeof(filepath), "players/%s", dir->d_name); // path lengkap file asal
                sscanf(dir->d_name, "Cavaliers-%2s", position); // posisi pemain dari nama file (point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C))

                char position_folder[32]; // savee path folder berdasarkan posisi
                snprintf(position_folder, sizeof(position_folder), "players/%s", position); // sv path folder berdasarkan posisi

                struct stat st = {0};
                if (stat(position_folder, &st) == -1) {
                    // memeriksa jika folder berdasarkan posisi sudah ada, jika tidak, maka buat folder baru
                    mkdir(position_folder, 0770);
                }

                char new_filepath[512]; // save file akhir
                snprintf(new_filepath, sizeof(new_filepath), "%s/%s", position_folder, dir->d_name); // save path file tujuan

                // mindahin file ke folder berdasarkan posisi pemain
                rename(filepath, new_filepath);
            }
        }
        closedir(d);
    }
}

//Hasil kategorisasi = jumlah pemain
void kategorisasi_posisi(const char *folderName) {
    int pg_count = 0;
    int sg_count = 0;
    int sf_count = 0;
    int pf_count = 0;
    int c_count = 0;

    struct dirent *dir;
    DIR *d;
    d = opendir(folderName);

    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (strstr(dir->d_name, "Cavaliers") != NULL && strstr(dir->d_name, ".png") != NULL) {
                char position[10];
                sscanf(dir->d_name, "Cavaliers-%2s", position);

                if (strcmp(position, "PG") == 0) {
                    pg_count++;
                } else if (strcmp(position, "SG") == 0) {
                    sg_count++;
                } else if (strcmp(position, "SF") == 0) {
                    sf_count++;
                } else if (strcmp(position, "PF") == 0) {
                    pf_count++;
                } else if (strcmp(position, "C-") == 0) {
                    c_count++;
                }
            }
        }
        closedir(d);

        // Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
        FILE *formasi_file = fopen("Formasi.txt", "w");
        if (formasi_file != NULL) {
            fprintf(formasi_file, "PG: %d\n", pg_count);
            fprintf(formasi_file, "SG: %d\n", sg_count);
            fprintf(formasi_file, "SF: %d\n", sf_count);
            fprintf(formasi_file, "PF: %d\n", pf_count);
            fprintf(formasi_file, "C: %d\n", c_count);
            fclose(formasi_file);
        } else {
            perror("Gagal membuka Formasi.txt 😭");
        }
    }
}
  
// pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7 ==> Kyrie Irving 
void majang_foto1(const char *folderName) {
    char sourcePath[256];
    char destinationPath[256];

    snprintf(sourcePath, sizeof(sourcePath), "%sPG/Cavaliers-PG-Kyrie-Irving.png", folderName);
    snprintf(destinationPath, sizeof(destinationPath), "clutch/Cavaliers-PG-Kyrie-Irving.png"); // "clutch" folder in the current directory

    int result = rename(sourcePath, destinationPath);

    if (result == 0) {
        printf("Pemindahan Kyrie Irving ke folder 'clutch' berhasil.\n");
    } else {
        perror("Gagal memindahkan Kyrie Irving ke folder 'clutch'");
    }
}

// The Block => LeBron James
void majang_foto2(const char *folderName) {
    char sourcePath[256];
    char destinationPath[256];

    snprintf(sourcePath, sizeof(sourcePath), "%sSF/Cavaliers-SF-LeBron-James.png", folderName);
    snprintf(destinationPath, sizeof(destinationPath), "clutch/Cavaliers-SF-LeBron-James.png"); // "clutch" folder in the current directory

    int result = rename(sourcePath, destinationPath);

    if (result == 0) {
        printf("Pemindahan LeBron James ke folder 'clutch' berhasil.\n");
    } else {
        perror("Gagal memindahkan LeBron James ke folder 'clutch'");
    }
}

// a) program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.
int main() {
    const char *folderName = "players";
    int status = mkdir(folderName, 0755);

    if (status == 0 || errno == EEXIST) {
        printf("Folder \"%s\" telah berhasil dibuat atau sudah ada.\n", folderName);
    } else {
        perror("Gagal membuat folder atau folder sudah ada");
        exit(EXIT_FAILURE);
    
    }

    // Panggil fungsi ada dari line 240-259
    donload();

    uzip("players.zip", "players");

    hapus();

    filter_pemain("players");

    kategorisasi_posisi("players");

    pindah_posisi("players");

    mkdir("clutch", 0755);

    majang_foto1("players/");

    majang_foto2("players/");

    return 0;
}