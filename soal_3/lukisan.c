#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>

void downloadImages(const char* folder);
void zipAndDelete(const char* folder);

void generateKiller(const char *mode) {
   // Menyimpan kode killer dalam file "killer.sh"
   FILE* kp = fopen("killer.sh", "w");
   if (kp == NULL) {
       perror("fopen");
       exit(EXIT_FAILURE);
   }

   fprintf(kp, "#!/bin/bash\n");
   if (strcmp("-a", mode) == 0) {
       fprintf(kp, "killall -9 lukisan\nrm $0\n");
   } else if (strcmp("-b", mode) == 0) {
       fprintf(kp, "kill_parent() {\n");
       fprintf(kp, "local parent_pid=$1\n");
       fprintf(kp, "kill $parent_pid\n");
       fprintf(kp, "}\n");
       fprintf(kp, "kill_parent $(pgrep lukisan)\nrm $0\n");
   }

   fclose(kp);

   pid_t pid = fork();
   if (pid < 0){
       perror("fork");
       exit(EXIT_FAILURE);
   }
   if (pid == 0){
       execlp("/bin/chmod", "chmod", "+x", "killer.sh", NULL);
       perror("execlp");
       exit(EXIT_FAILURE);
   }
   while(wait(NULL) != pid);
}

void createFolder() {
   char timestamp[20];
   char folder[30];
   time_t t = time(NULL);
   struct tm* tm_now = localtime(&t);
   strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", tm_now);
   snprintf(folder, sizeof(folder), "%s", timestamp);

   pid_t cid;
    cid = fork();

    if (cid < 0) {
        exit(EXIT_FAILURE);
    }

    if (cid == 0) {
        if (fork() == 0) {
            char *mkdir_argv[] = {"mkdir", "-p", folder, NULL};
            execv("/bin/mkdir", mkdir_argv);
            perror("execv");
            exit(EXIT_FAILURE);
        } else {
            int stat;
            while (wait(&stat) > 0);

            downloadImages(folder);
            zipAndDelete(folder);
        }
    }
}

void downloadImages(const char* folder) {
    if (chdir(folder) == -1) {
        perror("chdir");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 15; i++) {
       char timestamp[20];
       char url[100];
       char filename[50];
       time_t t = time(NULL);
       int size = (int)t % 1000 + 50;
       strftime(timestamp, sizeof(timestamp), "%Y-%m-%d_%H:%M:%S", localtime(&t));
       snprintf(url, sizeof(url), "https://source.unsplash.com/%dx%d", size, size);
       snprintf(filename, sizeof(filename), "%s.jpg", timestamp);

       pid_t pid = fork();
       if (pid == 0) {
           char *args[] = {"wget", url, "-qO", filename, NULL};
           execvp("/usr/bin/wget", args);
           perror("execvp");
           exit(EXIT_FAILURE);
       } else {
           sleep(5);
       }
   }
   zipAndDelete(folder);
}

void zipAndDelete(const char* folder) {
    chdir("..");

    char zip[100];
    snprintf(zip, sizeof(zip), "%.90s.zip", folder);

    pid_t pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        char *args[] = {"zip", "-qrm", zip, folder, NULL};
        execv("/usr/bin/zip", args);
        perror("execv");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
    }
}


int main(int argc, char *argv[]) {
   if (argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)) {
       fprintf(stderr, "Usage: %s <-a/-b>\n", argv[0]);
       exit(EXIT_FAILURE);
   }

   generateKiller(argv[1]);

   pid_t pid = fork(), sid;
   if (pid < 0) {
       exit(EXIT_FAILURE);
   }
   if (pid > 0) {
       exit(EXIT_SUCCESS);
   }

   umask(0);

   sid = setsid();
   if (sid < 0) {
       exit(EXIT_FAILURE);
   }

   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);

   while (1) {
       createFolder();
       sleep(30);
   }

   return 0;
}
